## `secpick` script

This is a small script that helps cherry-picking across multiple releases. It will stop
if there is a conflict while cherry-picking, otherwise will push the change to GitLab Security.

The list of options available running:

```
$ bin/secpick --help
```

For example:

```
bin/secpick -v 10.6 -b security-fix-mr-issue -s SHA
```

You can also pick a range of commits. To pick a range from `aaaa` (inclusive)
to `bbbb` you can:


```
bin/secpick -v 10.6 -b security-fix-mr-issue -s aaaa^..bbbb
```

It will change local branches to push to a new security branch for each specified release,
meaning that local changes should be saved prior to running the script.


