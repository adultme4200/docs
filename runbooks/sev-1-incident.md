# Overview

This document serves as an action reference manual for release managers during the highest severity issue affecting availability of GitLab.com.

Current definition of [availability S1][availability] states that we are seeing an immediate impact on customer's business goals and that the problem needs to be resolved with highest priority.

## S1 incident

As an example of S1 outage, we'll reference a scenario where post application deployment to GitLab.com, one of the more important workloads such as CI pipelines is no longer working for everyone on the platform.

At the start of the incident we would have:

1. Initiated incident in #incident-management Slack channel.
1. SRE on call, communications manager, current release manager, developer on call, and GitLab.com support member in the Incident Zoom room.

If any of the above are missing, ensure that you speak up and invite the missing people or create the missing resources.

As a release manager, your tasks would consist of providing sufficient background on tooling and changes that were introduced into the environment.

This means:

1. Provide the commit diff that was deployed to production.
    * Eg. use `/chatops auto_deploy status` to find the latest branch running in production and link that to the developer on call for further investigation, or check the [grafana dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m) to find the latest deployed commit.
1. Advise on the possible [remediation options](#remediation-options).
1. Define [a timeline](#timeline) for specific remediation options chosen in the incident discussion.
    * Eg. Think about what can be done immediately, what can be left for a couple of hours later, and what should be excluded from the conversation.  


### Remediation options

Consider the following options in combination with a [preferred timelines](#timeline):

1. **The source** of the problem **is unknown**
    * Consult [feature flag log](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=host%3A%3Agitlab.com) and consider disabling any recently enabled features.
  * [Deployment rollback](../general/deploy/gitlab-com.deployer.md#creating-a-new-deployment-for-rolling-back-gitlab). If the deployment contains [background database migrations], this option should be excluded.
1. **The source** of the problem **is known**
    * Leverage the [hot-patcher](../general/deploy/post-deployment-patches.md) to patch production fleet. In cases where the source of the issue is a binary component, or a service running on Kubernetes, hot-patcher should be excluded as an option. OR
    * Revert the change that caused the issue and pick the revert into the auto-deploy branch. It is ok to tag immediately after picking without waiting for pipelines to pass on the alternative repository (likely on dev.gitlab.org) using the security release process pipeline override. If using this option, ensure that you confirm that all specs have passed prior to deploying to production.

### Timeline

It is important to note that in situations such as this one, focus should be **exclusively** on reverting to the previously known working state.

Ensure that you challenge any decision that would consider creating the fix for the problem (if the fix ends up being broken, time was lost and now two problems exist). There are edge cases where it is ok to consider this, but those should be very rare.

In the first hour of the incident, it is common to consider the following options:

1. Disabling feature flags
1. Environment hot patch
1. Deployment rollback

Past the first hour, the options to consider are:

1. Reverting the offending code
1. Creating a new deployment


[availability]: https://about.gitlab.com/handbook/engineering/performance/#availability
[background database migrations]: ./background-migrations.md
